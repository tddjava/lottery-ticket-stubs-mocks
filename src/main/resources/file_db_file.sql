/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  selvy
 * Created: 6 Sep 2021
 */

create database files;

use files;

create table file_info
(
    id INT NOT NULL AUTO_INCREMENT,
    name varchar(128) NOT NULL,
    PRIMARY KEY (id)
);

create table line_in_file
(
    line_no INT NOT NULL AUTO_INCREMENT,
    file_id int,
    line_of_text CHAR(255),
    PRIMARY KEY (line_no),
    CONSTRAINT FK_file_id FOREIGN KEY (file_id)
        REFERENCES file_info(id)
);
