/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.lotterystubmockingmvn;

/**
 *
 * @author Selvyn
 */
public interface INumberGenerator
{
    Integer generate( int limit );
}
