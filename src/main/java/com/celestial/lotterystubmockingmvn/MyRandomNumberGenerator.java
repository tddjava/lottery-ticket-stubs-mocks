/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.lotterystubmockingmvn;

import java.util.Random;

/**
 *
 * @author Selvyn
 */
public class MyRandomNumberGenerator    implements INumberGenerator
{
    private final Random rand = new Random();
            
    @Override
    public Integer generate(int limit)
    {
        return rand.nextInt(limit);
    }
    
}
