/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.lotterystubmockingmvn;

/**
 *
 * @author Selvyn
 */
public class NumberGeneratorStub    implements  INumberGenerator
{
    private int number = -1;
    
    @Override
    public Integer generate(int limit)
    {
        return ++number;
    }
    
}
