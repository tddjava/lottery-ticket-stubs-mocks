/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.lotterystubmockingmvn;

import com.celestial.lotterystub.jmock.LotteryHandler;
import java.util.HashSet;

/**
 *
 * @author Selvyn
 */
public class MainUnit
{    
    static  public  void    main( String args[] )
    {
        NumberGeneratorStub ngs = new NumberGeneratorStub();
        LotteryHandler lotto = new LotteryHandler(ngs);
        
        HashSet<Integer> nums = lotto.GenerateRandomSet();
        
        System.out.println( nums );
        
        if( lotto.format(nums).equals("1 - 2 - 3 - 4 - 5"))
            System.out.println("They are equal");
   }
}
