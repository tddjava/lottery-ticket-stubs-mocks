/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.lotterystub.mockito;

/**
 *
 * @author selvy
 */
public class ChildClass 
{
    private GrandChild grandChild;

    public ChildClass(GrandChild grandChild)
    {
        this.grandChild = grandChild;
    }
    
    public  void    fooBar(int val)
    {
        System.out.println( val );
    }

    public  String    fooBar(String val)
    {
        System.out.println( val );
        
        return "[" + val + "]";
    }

    public  String  callGrandChild( String val )
    {
        return "{" + grandChild.fooBar(val) + "}";
    }
}
