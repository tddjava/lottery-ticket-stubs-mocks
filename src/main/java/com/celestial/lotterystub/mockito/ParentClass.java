/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.lotterystub.mockito;

/**
 *
 * @author selvy
 */
public class ParentClass 
{
    private ChildClass childClass;

    public ParentClass(ChildClass childClass) {
        this.childClass = childClass;
    }
    
    
    public void onTrade(int val)
    {
        this.fooBar(val);
    }
    
    public  void    fooBar( int val )
    {
        this.childClass.fooBar(val);        
    }

    public  String    fooBar( int val, String msg )
    {
        String result = "";
        
        if( val > 5 )
            result = this.childClass.fooBar(msg);        
        
        return result;
    }
    
    public  String  callChild( String val )
    {
        String result = childClass.callGrandChild(val);
        return "*" + result + "*";
    }
}
