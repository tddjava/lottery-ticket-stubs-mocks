/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.lotterystub.mockito;

/**
 *
 * @author selvy
 */
public class GrandChild
{
    public  void    fooBar(int val)
    {
        System.out.println( val );
    }

    public  String    fooBar(String val)
    {
        System.out.println( val );
        
        return "[" + val + "]";
    }    
}
