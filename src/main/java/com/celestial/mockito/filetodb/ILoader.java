/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.mockito.filetodb;

import java.util.List;

/**
 *
 * @author selvy
 */
@FunctionalInterface
public interface ILoader 
{
    List<String>    loadFile(String fname);
}
