/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.mockito.filetodb;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author selvy
 */
public class DBConnector
{
    private final IDbConnectionMgr connectionMgr;
    private Connection theConnection;
    private int fileId = 1;
    private int itsLineCount;
    private static final String SQL_INSERT_LINE = "insert into files.line_in_file" +
         " values (default, ?, ?);";
    private static final String SQL_LINE_COUNT = "select count(*) from files.line_in_file;";
    
    DBConnector(IDbConnectionMgr connection)
    {
        connectionMgr = connection;
    }

    Connection  openConnection(String dbEndPoint, String dbName) throws SQLException
    {
        theConnection = connectionMgr.openConnection(dbEndPoint, dbName);
        
        itsLineCount = queryLinesInDB();
        
        return theConnection;
    }
    
    public  int     getLineCount()
    {
        return itsLineCount;
    }
    
    public  void    updateLineCount()
    {
        updateLineCount(getLineCount() + 1);
    }
    
    public  void    updateLineCount(int value)
    {
        itsLineCount = value;
    }
    
    public  int getFileId()
    {
        return fileId;
    }

    int queryLinesInDB()
    {
        try
        {
            PreparedStatement ps = theConnection.prepareStatement(SQL_LINE_COUNT);

            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                itsLineCount = rs.getInt(1);
            }
            
        } catch (SQLException ex)
        {
            Logger.getLogger(DBConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
        return itsLineCount;
    }

    public int writeLine(String lineToWrite)
    {
        int fileId = getFileId();
        try
        {
            PreparedStatement ps = theConnection.prepareStatement(SQL_INSERT_LINE);
            ps.setInt(1, fileId);
            ps.setString(2, lineToWrite);
            ps.executeUpdate();
            
            updateLineCount();
            
        } catch (SQLException ex)
        {
            Logger.getLogger(DBConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
        return getLineCount();
    }
}
