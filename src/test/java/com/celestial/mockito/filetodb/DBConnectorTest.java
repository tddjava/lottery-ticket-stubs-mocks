/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.mockito.filetodb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.spy;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

/**
 *
 * @author selvy
 */
public class DBConnectorTest
{
    static String dbEndPoint = "jdbc:mysql://localhost:3306/";
    static String dbName = "files";
    static Connection sqlConn = Mockito.mock(Connection.class);
    // Mockito does not like the line below to be present in each test method
    // it registers the static method with the current test thread once, if this line
    // is called more than once in a unit test file, a Mockito exception is thrown
    //
    // The live DB connection code has been moved to it's own test file because
    // it was pickup the mocked DriverManager and returning null because the 
    // params being passed in (real values) did not match the stubbed values
    
    static MockedStatic<DriverManager> mockedDMgr = Mockito.mockStatic(DriverManager.class);
    
    @BeforeClass
    static public  void    beforeAllTestsDo()
    {
        // Because we are mocking the static, registration of the method should
        // only occur once for the unit test file
        mockedDMgr.when(() -> DriverManager.getConnection(dbEndPoint+dbName, "root", "ppp")).thenReturn(sqlConn);
    }

    
    @Test
    public void open_connection_mocked() throws SQLException 
    {
        // arrange
        IDbConnectionMgr connection = (endPoint, db) -> {
            Connection conn = DriverManager.getConnection(endPoint+db, "root", "ppp");
            return conn;
        };
        DBConnector cut = new DBConnector(connection);
        DBConnector spyCut = spy(cut);
        int expectedLineNoCount = 1;
        Mockito.doReturn(1).when(spyCut).queryLinesInDB();

        // act
        Connection conn = spyCut.openConnection(dbEndPoint, dbName);
        int lineNoResult = spyCut.getLineCount();

        // assert
        assertNotNull(conn);
        assertEquals(expectedLineNoCount, lineNoResult);
    }

    @Test
    public void write_line_to_mocked_connection() throws SQLException 
    {
        // arrange
        IDbConnectionMgr connection = (endPoint, db) -> {
            Connection conn = DriverManager.getConnection(endPoint+db, "root", "ppp");
            return conn;
        };
        DBConnector cut = new DBConnector(connection);

        DBConnector spyCut = spy(cut);
        Mockito.doReturn(0).when(spyCut).queryLinesInDB();

        PreparedStatement ps = Mockito.mock( PreparedStatement.class );
        Mockito.when(sqlConn.prepareStatement(Mockito.any())).thenReturn(ps);

        spyCut.openConnection(dbEndPoint, dbName);
        String lineToWrite = "be not changed by...";
        int expectedLineNo = 1;

        // act
        // The idea of writeLine() is that achg time a line is written to the DB
        // it's line No should be returned.  Line Nos start at 1
        int lineNo = spyCut.writeLine(lineToWrite);

        // assert
        assertEquals(expectedLineNo, lineNo);
    }

}
