/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.mockito.filetodb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

/**
 *
 * @author selvy
 */
public class LiveDBConnectorTest
{
    static String dbEndPoint = "jdbc:mysql://localhost:3306/";
    static String dbName = "files";
    
    
    // This test is not suitable for a build server. so we will need to stub
    // IDbConnectionMgr
    @Ignore
    @Test
    public void open_connection() throws SQLException 
    {
       // arrange
       String dbEndPoint_ = "jdbc:mysql://localhost:3306/";
       String dbName_ = "files";
       IDbConnectionMgr connection = (endPoint, db) -> {
           Connection conn = DriverManager.getConnection(endPoint+db, "root", "ppp");
           return conn;
       };
       DBConnector cut = new DBConnector(connection);

       // act
       Connection conn = cut.openConnection(dbEndPoint_, dbName_);

        // assert
       assertNotNull(conn);
    }

    // I'm ignoring the test because it keeps updating the DB
    @Ignore
    @Test
    public void write_line_to_live_connection() throws SQLException 
    {
        // arrange
        IDbConnectionMgr connection = (endPoint, db) -> {
            Connection conn = DriverManager.getConnection(endPoint+db, "root", "ppp");
            return conn;
        };
        DBConnector cut = new DBConnector(connection);
        cut.openConnection(dbEndPoint, dbName);
        String lineToWrite = "be not changed by...";
        int expectedLineNo = 1;

        // act
        // The idea of writeLine() is that each time a line is written to the DB
        // it's line No should be returned.  Line Nos start at 1
        int lineNo = cut.writeLine(lineToWrite);

        // assert
        assertEquals(expectedLineNo, lineNo);
    }
}
