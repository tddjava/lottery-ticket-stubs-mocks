/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.lotterystub.mockito;

import com.celestial.lotterystubmockingmvn.INumberGenerator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.HashSet;
import java.util.Iterator;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.junit.MockitoJUnitRunner;


/**
 *
 * @author selvy
 */
@RunWith(MockitoJUnitRunner.class)
public class LotteryHandlerTest 
{
    @Mock
    INumberGenerator    generator;
    
    public LotteryHandlerTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public  void    test_lottery_ticket_results()
    {
        // arrange
        LotteryHandler lotto = new LotteryHandler(generator);
        String expResult = "1 - 2 - 3 - 4 - 5";
        
        // act
        // Now we are ready to specify the expected behaviour
        when( generator.generate(LotteryHandler.HIGHEST_NUMBER))
                .thenReturn(0)
                .thenReturn(1)
                .thenReturn(2)
                .thenReturn(3)
                .thenReturn(4);
        
        HashSet<Integer> nums = lotto.GenerateRandomSet();
        
        System.out.println( nums );
        
        assertEquals(expResult, lotto.format(nums));
    }
    
    @Test
    public  void    test_lottery_ticket_results_breakdown()
    {
        // arrange
        LotteryHandler lotto = new LotteryHandler(generator);
        String expResult = "1 - 2 - 4 - 5 - 6";
        
        // act
        // Now we are ready to specify the expected behaviour
        when( generator.generate(LotteryHandler.HIGHEST_NUMBER))
                .thenReturn(0)
                .thenReturn(1)
                .thenReturn(3)
                .thenReturn(4)
                .thenReturn(5);
        
        HashSet<Integer> nums = lotto.GenerateRandomSet();
        
        // This is a way of spying on the CUTs behaviour
        // To test this, in the GenerateRandonSet() add an extra call to
        // generator.generate(HIGHEST_NUMBER); after the while loop, 
        // the verify will fail
        verify( generator, times(5)).generate(LotteryHandler.HIGHEST_NUMBER);
        System.out.println( nums );
        
        assertEquals(expResult, lotto.format(nums));
   }
    
    @Test
    public  void    test_lottery_ticket_results_breakdown_v2()
    {
        // arrange
        LotteryHandler lotto = new LotteryHandler(generator);
        String expResult = "1 - 2 - 3 - 4 - 5";
        HashSet<Integer> nums = mock(HashSet.class);
        Iterator<Integer> iter = mock(Iterator.class);
        
        // act
        // Now we are ready to specify the expected behaviour
        when( generator.generate(LotteryHandler.HIGHEST_NUMBER))
                .thenReturn(0)
                .thenReturn(1)
                .thenReturn(3)
                .thenReturn(4)
                .thenReturn(5);
        
        HashSet<Integer> actualResult = lotto.GenerateRandomSet();
        System.out.println( actualResult );
        
        //doReturn(iter).when(actualResult).iterator();
        when(nums.iterator()).thenReturn(iter);
        when(nums.size()).thenReturn(5);
        when(iter.hasNext())
                .thenReturn(true)
                .thenReturn(true)
                .thenReturn(true)
                .thenReturn(true)
                .thenReturn(true)
                .thenReturn(false);
        when(iter.next())
                .thenReturn(1)
                .thenReturn(2)
                .thenReturn(3)
                .thenReturn(4)
                .thenReturn(5);
        
        String actualString = lotto.format(nums);
        
        assertEquals(expResult, actualString);
   }
    
   @Test
   public   void    example_simple_mock()
   {
        // arrange
        
        // Simple example of the mock being told what to return
        Integer result = generator.generate(LotteryHandler.HIGHEST_NUMBER);
        assertEquals(new Integer(0), result);
   }
        
   @Test
   public   void    mocking_on_chained_returns()
   {
        // arrange
        LotteryHandler lotto = new LotteryHandler(generator);
        String expResult = "1 - 2 - 4 - 5 - 6";
        /*
         * You can't chanin the .thenReturn() method as you would if you were expecting
         * obj.method1().method2() because .thenReturn() always returns 
         * OngoingStubbing<T>
         */
        
        // act
        // Now we are ready to specify the expected behaviour
        when( generator.generate(LotteryHandler.HIGHEST_NUMBER))
                .thenReturn(0)
                .thenReturn(1)
                .thenReturn(3)
                .thenReturn(4)
                .thenReturn(5);
        
        HashSet<Integer> nums = lotto.GenerateRandomSet();
        
        // This is a way of spying on the CUTs behaviour
        // To test this, in the GenerateRandonSet() add an extra call to
        // generator.generate(HIGHEST_NUMBER); after the while loop, 
        // the verify will fail
        verify( generator, times(5)).generate(LotteryHandler.HIGHEST_NUMBER);
        System.out.println( nums );
        
        assertEquals(expResult, lotto.format(nums));
   }
}
