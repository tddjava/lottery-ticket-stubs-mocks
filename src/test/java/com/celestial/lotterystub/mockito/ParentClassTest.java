/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.lotterystub.mockito;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;
import org.mockito.junit.MockitoJUnitRunner;


/**
 *
 * @author selvy
 */
@RunWith(MockitoJUnitRunner.class)
public class ParentClassTest 
{
    @Test
    public void mock_on_child_class()
    {
        // arrange        
        ChildClass mock = mock(ChildClass.class);

        // act
        mock.fooBar(1);
        
        // assert
        verify( mock ).fooBar(1);
    }

    @Test
    public void mock_on_child_but_used_by_parent()
    {
        // arrange        
        String input = "hello";
        String input_2 = "hello world";
        String input_3 = "Selvyn says hello world";
        ChildClass mock = mock(ChildClass.class);
        ParentClass cut = new ParentClass( mock );
        
        // act
        String result = cut.fooBar(6, input_2);
        
        // assert
        verify( mock ).fooBar(input_2);
    }

    @Test
    public void mock_on_child_but_used_by_parent_test_result()
    {
        // arrange        
        String input = "hello";
        String input_2 = "hello world";
        String inputReturn = "["+input+"]";
        ChildClass mock = mock(ChildClass.class);
        ParentClass cut = new ParentClass( mock );        
        // You only need to use the when, if you need to stub the data
        when(mock.fooBar(input)).thenReturn(inputReturn);
        
        // act
        String result = cut.fooBar(6, input);
        
        // assert
        assertEquals( inputReturn, result );
    }

    //@Ignore
    @Test
    public void failing_test_mock_on_child_but_used_by_parent_test_result()
    {
        // arrange        
        String input = "hello";
        String input_2 = "hello world";
        String inputReturn = "["+input+"]";
        ChildClass mock = mock(ChildClass.class);
        ParentClass cut = new ParentClass( mock );
        // You only need to use the when, if you need to stub the data
        lenient().when(mock.fooBar(input)).thenReturn(inputReturn);
        
        // act
        String result = cut.fooBar(6, input_2);
        
        // assert
        assertNotEquals( inputReturn, result );
    }

    @Test
    public void spy_on_child_and_mock_gc_but_used_by_parent_test_result()
    {
        // arrange        
        String input = "hello";
        String parentResult = "*{["+input+"]}*";
        String childResult = "{["+input+"]}";
        GrandChild gmock = mock(GrandChild.class);
        ChildClass child = new ChildClass( gmock );
        ChildClass childSpy = spy(child);
        ParentClass cut = new ParentClass( childSpy );
        String grandChildResult = "["+input+"]";
        when(gmock.fooBar(input)).thenReturn(grandChildResult);
        //Mockito.doReturn(childResult).when(childSpy).callGrandChild(input);
        
        // act
        String result = cut.callChild(input);
        
        // assert
        verify(childSpy, times(1)).callGrandChild(input);
        assertEquals( parentResult, result );
    }

    @Test
    public void mock_and_spy_on_child_and_mock_gc_both_used_by_parent_test_result()
    {
        // arrange        
        String input = "hello";
        String parentResult = "*{["+input+"]}*";
        String childResult = "{["+input+"]}";

        // This local variable is not required, I'm testing the parent and it's 
        // interactions with the child, to test any further is crazy because
        // that should be a unit test on the child
        //String grandChildResult = "["+input+"]";

        GrandChild gmock = mock(GrandChild.class);
        // gmock isn't actually need, null could be passed in
        ChildClass child = new ChildClass( gmock );
        ChildClass childSpy = spy(child);
        ParentClass cut = new ParentClass( childSpy );
        Mockito.doReturn(childResult).when(childSpy).callGrandChild(input);

        // Specifying the when here makes no sense, I'm testing the parent and it's 
        // interactions with the child, to test any further is crazy because
        // that should be a unit test on the child
        //when(gmock.fooBar(input)).thenReturn(grandChildResult);
        
        // act
        String result = cut.callChild(input);
        
        // assert
        verify(childSpy, times(1)).callGrandChild(input);
        assertEquals( parentResult, result );

        // testing this makes no sense, I'm testing the parent and it's 
        // interactions with the child, to test any further is crazy because
        // that should be a unit test on the child
        //verify(gmock, times(1)).fooBar(input);
    }

    @Ignore
    @Test
    public void mock_child_and_gc_both_used_by_parent_test_result()
    {
        // This code is none sensical
        // I'm asking a mock to invoke a mock
        // A mock has no method so it cannot invoke another method, also if it
        // did have a method what would be that method's behaviour, it's a mock
        // arrange        
        String input = "hello";
        String parentResult = "*{["+input+"]}*";
        String childResult = "{["+input+"]}";
        String grandChildResult = "["+input+"]";
        GrandChild gmock = mock(GrandChild.class);
        ChildClass child = mock(ChildClass.class);
        //ChildClass childSpy = spy(child);
        ParentClass cut = new ParentClass( child );
        // You only need to use the when, if you need to stub the data
        when(child.callGrandChild(input)).thenReturn(childResult);
        //Mockito.doReturn(childResult).when(childSpy).callGrandChild(input);
        //Mockito.doReturn(grandChildResult).when(gmock).fooBar(input);
        when(gmock.fooBar(input)).thenReturn(grandChildResult);
        
        // act
        String result = cut.callChild(input);
        
        // assert
        verify(child, times(1)).callGrandChild(input);
//        verify(gmock, times(1)).fooBar(input);
        assertEquals( parentResult, result );
    }

    @Test
    public void spy_on_child_class()
    {
        // arrange
        ParentClass cut = new ParentClass( new ChildClass(null) );
        ParentClass spy = spy(cut);

        spy.onTrade(1);
        // act
        
        verify( spy ).fooBar(1);
        
        verify( spy, times(1)).fooBar(1);
    }

    @Test
    public void verify_spy_on_child_class()
    {
        // arrange
        ChildClass cc = new ChildClass(null);
        ChildClass ccSpy = spy( cc );
        ParentClass cut = new ParentClass( ccSpy );

        cut.onTrade(1);
        // act
        
        verify( ccSpy ).fooBar(1);
        
        verify( ccSpy, times(1)).fooBar(1);
    }
}
