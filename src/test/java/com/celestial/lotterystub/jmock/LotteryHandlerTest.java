/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.lotterystub.jmock;

import com.celestial.lotterystubmockingmvn.INumberGenerator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.HashSet;
import static org.jmock.AbstractExpectations.onConsecutiveCalls;
import static org.jmock.AbstractExpectations.returnValue;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JUnit4Mockery;


/**
 *
 * @author selvy
 */
public class LotteryHandlerTest 
{
    Mockery context = new JUnit4Mockery();
    INumberGenerator    generator;
    
    public LotteryHandlerTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public  void    test_lottery_ticket_results()
    {
        // arrange
        generator = context.mock(INumberGenerator.class);
        LotteryHandler lotto = new LotteryHandler(generator);
        String expResult = "1 - 2 - 3 - 4 - 5";
        
        // act
        // Now we are ready to specify the expected behaviour
        context.checking(new Expectations()
        {{
            exactly(LotteryHandler.LOTTERY_SIZE).of(generator);
            will( onConsecutiveCalls(   returnValue(0),
                                        returnValue(1),
                                        returnValue(2),
                                        returnValue(3),
                                        returnValue(4)));
        }});
        
        HashSet<Integer> nums = lotto.GenerateRandomSet();
        
        System.out.println( nums );
        
        assertEquals(expResult, lotto.format(nums));
    }
    
    @Test
    public  void    test_lottery_ticket_results_breakdown()
    {
        // arrange
        generator = context.mock(INumberGenerator.class);
        LotteryHandler lotto = new LotteryHandler(generator);
        String expResult = "1 - 2 - 4 - 5 - 6";
        
        // act
        // Now we are ready to specify the expected behaviour
        Expectations exp = new Expectations();
        
        exp.exactly(LotteryHandler.LOTTERY_SIZE).of(generator)
                .generate(LotteryHandler.HIGHEST_NUMBER);
        exp.will( onConsecutiveCalls(   returnValue(0),
                                        returnValue(1),
                                        returnValue(3),
                                        returnValue(4),
                                        returnValue(5)));
        
        context.checking(exp);
        
        HashSet<Integer> nums = lotto.GenerateRandomSet();
        
        System.out.println( nums );
        
        assertEquals(expResult, lotto.format(nums));
   }
}
